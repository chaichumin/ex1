import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Test1 from './componancts/Test1/Test1';
import Questions from './componancts/Questions/Questions';


ReactDOM.render(
  <React.StrictMode>
    <Test1 />
    <Questions/>
  </React.StrictMode>,
  document.getElementById('root')
);
reportWebVitals();

import React from 'react';
import { Card, Radio, Space } from 'antd';
import '../../index.css'




const myQuestions = [
  {
    question: "1 + 2 is ?",
    answers: ["1", "2", "3"],
    correctAnswer: 2
  },
  {
    question: "What is the best site for Web Programmer ?",
    answers: ["Stack Overflow", "Quora", "w3school"],
    correctAnswer: 0
  },
  {
    question: "Who is Prime minister fo Thailand ?",
    answers: ["Prayut Chan-o-cha", "Yingluck Shinawatra", "Abhisit Vejjajiva", "Somchai Wongsawat"],
    correctAnswer: 0
  }
];


const radiocheck = (e, index, Answer) => {
  var value = e.target.value
  if (value != Answer)
  {
    alert("Incorect")
  }
  

}

const mapdata = () => {
  return myQuestions.map(
    function (myQuestions, index) {
      return (
        <>
          <Card title={myQuestions.question} style={{ width: 500, borderRadius: 10, marginTop: 20 }}>
            <Radio.Group name="radiogroup" onChange={(e) => {
              radiocheck(e, index, myQuestions.correctAnswer)
            }} >
              <Space direction="vertical">
                {
                  myQuestions.answers.map(
                    (choice, index) => {
                      return (
                        <>
                          <Radio value={index}>{choice}</Radio>
                        </>
                      )
                    }
                  )

                }
              </Space>

            </Radio.Group>
          </Card>
        </>
      )
    }
  )
}

function Questions() {

  return (
    <div  >
      {mapdata()}
    </div>
  );
}

export default Questions;

import React, {useState } from 'react';
import { Button } from 'antd';



function Test1(){

  const [txt, Settxt] = useState(" ");

  const showTxtButton = (txt) => {
    Settxt(txt = txt);
    console.log(txt)
    if (txt == "Morning") {
      alert("Good Morning")
    }
    if(txt == "Afternoon") {
      alert("Good Afternoon")
    }
    if(txt == "Night") {
      alert("Good Night")
    }
    if(txt == "Click") {
      alert("Don’t Click Me")
    }
    
  }
    return (
    <div>

      <Button onClick={() => { showTxtButton("Morning")}}>Good Morning</Button>
      <Button onClick={() => { showTxtButton("Afternoon") }}>Good Afternoon</Button>
      <Button onClick={() => { showTxtButton("Night") }}>Good Night</Button>
      <Button onClick={() => { showTxtButton("Click") }}>Don’t Click Me</Button>

    </div>
  );
}

export default Test1;
